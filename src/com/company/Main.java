package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        //REMOVE DUPLICATES FROM ARRAY LIST
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(1);

        ArrayList<Integer> newList = new ArrayList<Integer>();

        for (Integer element : arrayList) {

            if (!newList.contains(element)) {

                newList.add(element);
            }
        }
        System.out.println("After removing duplicates from array list : " + newList);

        // LINKED LIST ADDING AND SORTING(AFTER ADDING)
        LinkedList<Integer> linkedList1 = new LinkedList<>();
        linkedList1.add(1);
        linkedList1.add(5);
        linkedList1.add(3);

        LinkedList<Integer> linkedList2 = new LinkedList<>();
        linkedList2.add(2);
        linkedList2.add(6);
        linkedList2.add(4);

        linkedList1.addAll(linkedList2);
        Collections.sort(linkedList1);
        System.out.println("Lined list merging : " + linkedList1);


        //HASH MAP
        HashMap<Integer, Integer> hashMap = new HashMap();
        hashMap.put(1, 1);
        hashMap.put(2, 5);
        hashMap.put(3, 3);
        hashMap.put(4, 6);
        hashMap.put(5, 5);
        hashMap.put(6, 2);
        hashMap.put(7, 3);
        hashMap.put(8, 7);
        hashMap.put(9, 3);
        hashMap.put(10, 8);
        hashMap.put(11, 3);


        LinkedList<Integer> linkedList = new LinkedList();
        linkedList.add(1);
        linkedList.add(5);
        linkedList.add(3);
        linkedList.add(6);
        linkedList.add(5);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(7);
        linkedList.add(3);
        linkedList.add(8);
        linkedList.add(3);

        //SORTING HASH MAP
        System.out.println("Sorted hash map : " + sortHashMap(linkedList));

        //CHECK PARENTHESES PROBLEM
        char expression[] = {'{', '(', ')', '}', '[', ']'};
        if (areParenthesesBalanced(expression))
            System.out.println("Parentheses are Balanced ");
        else
            System.out.println("Parentheses are not Balanced ");

    }

    public static LinkedList<Integer> sortHashMap(LinkedList<Integer> input) {
        LinkedHashMap<Integer, Integer> inputMap = new LinkedHashMap<>();
        for (Integer key : input) {
            if (inputMap.get(key) == null) {
                inputMap.put(key, 1);
            } else {
                int newValue = inputMap.get(key) + 1;
                inputMap.replace(key, newValue);
            }

        }

        LinkedList<Integer> sortedList = new LinkedList<Integer>();
        for (Map.Entry<Integer, Integer> entry : inputMap.entrySet()) {
            if (entry.getValue() == 1) {
                sortedList.add(entry.getKey());
            } else {
                for (int index = 0; index < entry.getValue(); index++) {
                    sortedList.addFirst(entry.getKey());
                }
            }
        }
        return sortedList;


    }

    static boolean areParenthesesBalanced(char expression[]) {
        Stack stack = new Stack();

        for (int i = 0; i < expression.length; i++) {
            if (expression[i] == '{' || expression[i] == '(' || expression[i] == '[')
                stack.push(expression[i]);

            if (expression[i] == '}' || expression[i] == ')' || expression[i] == ']') {
                if (stack.isEmpty()) {
                    return false;
                } else if (!isMatchingPair((Character) stack.pop(), expression[i])) {
                    return false;
                }
            }

        }

        if (stack.isEmpty())
            return true;
        else {
            return false;
        }
    }

    static boolean isMatchingPair(char character1, char character2) {
        if (character1 == '(' && character2 == ')')
            return true;
        else if (character1 == '{' && character2 == '}')
            return true;
        else if (character1 == '[' && character2 == ']')
            return true;
        else
            return false;
    }


}


